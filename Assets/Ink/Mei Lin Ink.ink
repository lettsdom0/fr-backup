->MEILIN_APP1_INTRO

VAR currentKnot = "none"
VAR nextKnot = "none"
VAR currentEmotion = "none"
VAR appDone = "false"
VAR resultDone = "false"
VAR canChoiceBeMade = "no"
VAR weekDone = "false"

==MEILIN_APP1_INTRO==

~ currentKnot = "MEILIN_APP1_INTRO"
~ resultDone = "false"
~currentEmotion = "neutral"
Hello, I’m Mei Lin. I brought you a bouquet from my flower shop. It’s just around the corner, we’ve been in business for over forty years.

I’ve lived in this neighborhood my whole life, so if you ever need anything at all you can come to me or my wife, Anya.

I can’t wait to get to know you better! You must lead such an interesting life as an apothecary. I can’t imagine all the wonderful places you’ve been.

I would love to hear what kinds of plants you work with sometime!

Oh, I got so excited I almost forgot, I need to ask you for something for my joints. I’ve been having pain, especially in my hands, that has been interfering with my ability to garden and knit.

I’d love to try one of your potions. Do you have one that can soothe these old bones?


~ nextKnot = "APP1_Con"

->APP1_Con

==APP1_Con==

~ currentKnot = "APP1_Con"
~ canChoiceBeMade = "yes"

*   Body
    ~ currentEmotion = "happy"
    Thank you! What a great addition you’ll be to the community! /*happy*/
    ~ nextKnot = "MEILIN_APP1_RESULT_A"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    ~ currentEmotion = "happy"
    Thank you! What a great addition you’ll be to the community! /*happy*/
    ~ nextKnot = "MEILIN_APP1_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    ~ currentEmotion = "happy"
    Thank you! What a great addition you’ll be to the community! /*happy*/
    ~ nextKnot = "MEILIN_APP1_RESULT_C"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    ~ currentEmotion = "happy"
    Thank you! What a great addition you’ll be to the community! /*happy*/
    ~ nextKnot = "MEILIN_APP1_RESULT_D"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 1 Result A]
[Intellect: Appointment 1 Result B]
[Tranquility: Appointment 1 Result C]
[Charisma: Appointment 1 Result D]
*/


==MEILIN_APP1_RESULT_A==

~ currentKnot = "MEILIN_APP1_RESULT_A"
~ currentEmotion = "none"
~ appDone = "true"



I’ve been moving around so easily since I took your potion! I haven’t felt any pain at all.  /*happy*/

I’ve finally been able to get back in the garden and I still feel well enough to make progress on my knitting.

I can’t wait to tell everyone how wonderful your potions are!

~ weekDone = "true"
~ nextKnot = "MEILIN_APP2"
~ resultDone = "true"

->END

==MEILIN_APP1_RESULT_B==

~ currentKnot = "MEILIN_APP1_RESULT_B"
~ currentEmotion = "none"
~ appDone = "true"

~ currentEmotion = "happy"
Hello again! I devised a method for gardening that is less strenuous on my joints. 

Thank you for providing me the inspiration through your potion! 

I’m feeling some relief at the end of the day, definitely glad to have less pain.

I’ll be sure to bring some beautiful flowers soon!


~ weekDone = "true"
~ nextKnot = "MEILIN_APP2"
~ resultDone = "true"

->END


==MEILIN_APP1_RESULT_C==

~ currentKnot = "MEILIN_APP1_RESULT_C"
~ currentEmotion = "none"
~ appDone = "true"

Hello again. Your potion did not seem to do very much for me this time. My joints are still aching, but I’ve been able to keep my mind off it.

I’m still not able to give my work and hobbies as much attention as I would like, but I’ve managed to make some progress with my knitting.


~ weekDone = "true"
~ nextKnot = "MEILIN_APP2"
~ resultDone = "true"

->END

==MEILIN_APP1_RESULT_D==

~ currentKnot = "MEILIN_APP1_RESULT_D"
~ currentEmotion = "none"
~ appDone = "true"

~ currentEmotion = "neutral"
Hello again.  I’m still moving slow, but I can’t neglect my gardening. Though I appreciate you trying to help with your potions. 

A few neighborhood kids stopped by while I worked, and I was able to convince them to assist me in pulling weeds with a friendly competition. 


~ currentEmotion = "happy"
The winner brought home a free bouquet of flowers for their mom. These kids are really something. 

Normally they’re so busy running around they don’t give time to old ladies, but they seemed to have fun. 



~ weekDone = "true"
~ nextKnot = "MEILIN_APP2"
~ resultDone = "true"

->END




==MEILIN_APP2==

~ currentKnot = "MEILIN_APP2"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "none"
~ weekDone = "false"

~ currentEmotion = "happy"
Good morning. I hope you’ve been able to settle in and feel at home in our city. 

~ currentEmotion = "nervous"

I’m leading a community initiative, but it hasn’t gotten as much traction as I hoped it would. 


This neighborhood lacks a communal gathering place. I believe I can provide this with a community garden. 

Not many people in the city know how to garden, and it is such a fulfilling pastime, as well as a nice change of pace from bustling city life.

Tomorrow is the opening day for the garden. There is an event to begin planting, but no one has indicated they will come. 
~ currentEmotion = "neutral"

Do you think you could help me find a way to get more people interested?



~ nextKnot = "APP2_Con"

->APP2_Con

==APP2_Con==

~ currentKnot = "APP2_Con"
~ canChoiceBeMade = "yes"

*   Body
    ~ currentEmotion = "happy"
    Thank you! Let me know if there is anything you need to brew your potions! /*happy*/  
    ~ nextKnot = "MEILIN_APP2_RESULT_A" 

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    ~ currentEmotion = "happy"
    Thank you! Let me know if there is anything you need to brew your potions! /*happy*/
    ~ nextKnot = "MEILIN_APP2_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    ~ currentEmotion = "happy"
    Thank you! Let me know if there is anything you need to brew your potions!  /*happy*/
    ~ nextKnot = "MEILIN_APP2_RESULT_C"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma    
    ~ currentEmotion = "happy"
    Thank you! Let me know if there is anything you need to brew your potions! /*happy*/
    ~ nextKnot = "MEILIN_APP2_RESULT_D"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 2 Result A]
[Intellect: Appointment 2 Result B]
[Tranquility: Appointment 2 Result C]
[Charisma: Appointment 2 Result D]
*/


==MEILIN_APP2_RESULT_A==

~ currentKnot = "MEILIN_APP2_RESULT_A"
~ currentEmotion = "none"
~ nextKnot = "MEILIN_APP3"
~ appDone = "true"

~ currentEmotion = "happy"
Your potion made me feel decades younger! When no one showed up for opening day, I decided not to wait around any longer and tackle the garden myself. /*happy*/

 ~ currentEmotion = "mad"
 Unfortunately, your potion couldn’t last forever. My strength wore out, so I was only able to get half of the garden planted. /*mad*/

  ~ currentEmotion = "neutral"
It’s still more than I would have been able to accomplish if I didn’t have you help. /*neutral*/

~ currentEmotion = "happy"
But this was a good start to get people interested. Seeing the progress I made will help the community visualize the full potential of the garden. /*happy*/



~ resultDone = "true"

->END

==MEILIN_APP2_RESULT_B==

~ currentKnot = "MEILIN_APP2_RESULT_B"
~ currentEmotion = "neutral"
~ nextKnot = "MEILIN_APP3"
~ appDone = "true"

After taking your potion, I sat down to think of a solution to the weak reception to my community garden. /*neutral*/

I started offering a discount for my shop for anyone who turned up at the garden’s opening day to assist in planting.
~ currentEmotion = "happy"
This turned out to be a great motivator. Many came to help, and we were able to plant the whole garden! /*happy*/



I’m so glad to see people getting excited about our community again. 

~ resultDone = "true"

->END


==MEILIN_APP2_RESULT_C==

~ currentKnot = "MEILIN_APP2_RESULT_C"
~ currentEmotion = "neutral"
~ nextKnot = "MEILIN_APP3"
~ appDone = "true"

~ currentEmotion = "nervous"
Reception hasn’t improved for the community garden since I last visited. I haven’t even made much progress on the garden myself. /*nervous*/

~ currentEmotion = "mad"
Since I took your potion, I’ve felt a lack of ambition. I’m shocked, it’s out of character for me. 

 ~ currentEmotion = "neutral"
 It’s been a slow start for the garden, but I won’t give up on it. I have confidence it will become successful and be a great service for the community. /*neutral*/


~ resultDone = "true"

->END

==MEILIN_APP2_RESULT_D==

~ currentKnot = "MEILIN_APP2_RESULT_D"
~ currentEmotion = "neutral"
~ nextKnot = "MEILIN_APP3"
~ appDone = "true"

~ currentEmotion = "happy"
When I left after my last visit, I set out on a social campaign for the community garden, I went door to door to recruit participants for the opening day.

I was slow, but I wasn’t going to let that stop me.

You definitely helped people listen to an old lady ramble on. It turned out to be a success; everyone I talked to showed up for the festivities.

It was a great way to start building community connections! I’m glad you’ve joined us here. 



~ resultDone = "true"

->END






==MEILIN_APP3A==

~ currentKnot = "MEILIN_APP3A"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "neutral"

//Stopped editing script here ***************************************************

I hope you’re faring well. I haven’t rested easy since war was declared. The fighting is miles away from the city, but we have to be prepared for anything. /*sad*/

Now is not the time to settle, not even in safety. We can get through this tumultuous time if we stick together. /*mad*/

That’s why I’ve decided to take my garden initiative to the next step and create an organization to aid the community during wartime. /*neutral*/

I’m calling it the Homefront Action Movement! My goal is to make sure everyone is taken care of, that they have food, shelter, and do anything that can provide comfort. 

The response from the community has been encouraging! So many people are eager to participate. We’ve started meeting weekly at the garden. /*happy*/

I’m happy to know so many people care to be involved, but the movement has grown so fast it’s overwhelming. There are so many issues to address, I can’t keep track of them. /*nervous*/

The movement is far beyond the scale of anything I’ve ever managed before, even my shop. I have to spend all day and night meeting with people to ensure everything gets done. /*neutral*/

~ nextKnot = "APP3A_Con"

->APP3A_Con

==APP3A_Con==

~ currentKnot = "APP3A_Con"
~ canChoiceBeMade = "yes"

*   Body
    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 3A Result A]
[Intellect: Appointment 3A Result B]
[Tranquility: Appointment 3A Result C]
[Charisma: Appointment 3A Result D]
*/


==MEILIN_APP3A_RESULT_A==

~ currentKnot = "MEILIN_APP3A_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m still up all night, but now I have a boost of endurance to sustain myself and continue working through the day and the nights beyond. /*neutral*/

There’s so much to be done. My team has made great progress, but I won’t let a single call for aid go unacknowledged! /*happy*/


->END

==MEILIN_APP3A_RESULT_B==

~ currentKnot = "MEILIN_APP3A_RESULT_B"
~ currentEmotion = "neutral"
~ appDone = "true"

I’ve taken a step back from having a hand in everything myself. Instead, now the Homefront Action Movement has a system of delegation.  /*neutral*/
We now have designated team leaders to handle each of our significant task. There are teams for farming, food distribution, and childcare. 
Now everything can get done and we’ll be able to create more teams for any other needs that should arise. /*happy*/



->END


==MEILIN_APP3A_RESULT_C==

~ currentKnot = "MEILIN_APP3A_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m no longer overwhelmed. My mind is focused, and I can tackle issues one at a time without being distracted by other pressing matters. /*neutral*/

I’m better able to guide recruits to independently find solutions, and we’ve increased our productivity! /*happy*/


->END

==MEILIN_APP3A_RESULT_D==

~ currentKnot = "MEILIN_APP3A_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

Things are less chaotic. Everyone is working cooperatively and respecting me as the leader of the movement. /*neutral*/

It’s great to feel the comradery of community again! We’ve got some many people helping all over the city, I still can’t keep track of it all. /*happy*/


->END






==MEILIN_APP3B==

~ currentKnot = "MEILIN_APP3B"
~ appDone = "false"
~ currentEmotion = "neutral"

I hope you’re faring well, I haven’t rested easy since war was declared. The fighting is miles away from the city, but we have to be prepared for anything. /*nervous*/

Now is not the time to settle, not even in safety. But we can get through this tumultuous time if we stick together. /*mad*/

That’s why I’ve decided to take my garden initiative to the next step and create an organization to aid the community during wartime. /*neutral*/

I’m calling it the Homefront Action Movement! My goal is to make sure everyone is taken care of, that they have food, shelter, and do anything that can provide comfort.  

We meet every week in the garden. Recruitment has been slow. Those who are involved are enthusiastic, but there still aren’t enough members to tend to everyone’s needs.  /*nervous*/

The movement is far beyond the scale anything I’ve ever managed before, even my shop. I have to spend all day and night working to ensure everything gets done. /*neutral*/



~ nextKnot = "APP3B_Con"

->APP3B_Con

==APP3B_Con==

~ currentKnot = "APP3B_Con"
~ canChoiceBeMade = "yes"

*   Body
    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3B_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3B_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you!  /*happy*/
    ~ nextKnot = "MEILIN_APP3B_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3B_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 3B Result A]
[Intellect: Appointment 3B Result B]
[Tranquility: Appointment 3B Result C]
[Charisma: Appointment 3B Result D]
*/


==MEILIN_APP3B_RESULT_A==

~ currentKnot = "MEILIN_APP3B_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m still up all night, but now I have a boost of endurance to sustain myself and continue working through the day and the nights beyond. /*neutral*/

There’s so much to be done. I’m constantly running errands, even in the dead of night. /*nervous*/

I’ll must continue to manage until the Homefront Action Movement gains more recruits. I won’t let a single call for aid go unacknowledged! /*neutral*/



->END

==MEILIN_APP3B_RESULT_B==

~ currentKnot = "MEILIN_APP3B_RESULT_B"
~ currentEmotion = "neutral"
~ appDone = "true"

I’ve taken a step back from having a hand in everything myself. Instead, now the Homefront Action Movement has embraced a systems delegation. /*neutral*/

I want there be leaders for each of our significant task, but unfortunately, we still don’t have enough recruits to fill these roles. 

We’re more organized, but still slow to tend to all community needs. 



->END


==MEILIN_APP3B_RESULT_C==

~ currentKnot = "MEILIN_APP3B_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m no longer overwhelmed. My mind is focused, and I can tackle issues one at a time without being distracted by other pressing matters. /*happy*/

There’s still so much, but I’m making progress! 


->END

==MEILIN_APP3B_RESULT_D==

~ currentKnot = "MEILIN_APP3B_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I spent some time socializing at the marketplace this week, and I was able to recruit more for the Homefront Action Movement. /*neutral*/

Now I have enough members to take over some responsibilities. /*happy*/


->END

/*
Appointment 4 as multiple people
*/


==MEILIN_APP5A==

~ currentKnot = "MEILIN_APP5A"
~ appDone = "false"
~ currentEmotion = "neutral"

I could barely make it down the street it was crowded with people celebrating! We’ve gained land and pushed enemy troops away from the city! /*happy*/

Morale is as strong as ever in the Homefront Action Movement!

The war isn’t over yet though. The celebration has been a relief, but we still have a long road ahead of us.  We can’t lose focus now. /*neutral*/

We must never forget this victory was made possible through sacrifice. Many people lost loved ones. /*mad*/

I’m on my way to visit a family who lost their father in the battle. I hope there is something I can do to bring some comfort. /*sad*/



~ nextKnot = "APP5A_Con"

->APP5A_Con

==APP5A_Con==

~ currentKnot = "APP5A_Con"
~ canChoiceBeMade = "yes"

*   Body
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5A_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5A_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5A_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5A_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 5A Result A]
[Intellect: Appointment 5A Result B]
[Tranquility: Appointment 5A Result C]
[Charisma: Appointment 5A Result D]
*/


==MEILIN_APP5A_RESULT_A==

~ currentKnot = "MEILIN_APP5A_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I was able to help our neighbors by completing housework. It wasn’t much, but it provided relief.  You helped me feel recharged after a long day of work. /*sad*/


->END

==MEILIN_APP5A_RESULT_B==

~ currentKnot = "MEILIN_APP5A_RESULT_B"
~ currentEmotion = "neutral"
~ appDone = "true"

It’s impossible to outsmart grief, no matter how hard any of us tries. It must run its course. /*sad*/

As hard as it is to accept, all I can do is be a comforting presence for our neighbors. 


->END


==MEILIN_APP5A_RESULT_C==

~ currentKnot = "MEILIN_APP5A_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

I spent all night long talking with our neighbors. Thanks to you, my mind was clear of other worries, allowing my to be a comforting presence by listening. /*neutral*/


->END

==MEILIN_APP5A_RESULT_D==

~ currentKnot = "MEILIN_APP5A_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I was able to assist our neighbors by keeping the children entertained by telling stories while the mom was able to spend time with her sister. /*neutral*/


->END


==MEILIN_APP5B==

~ currentKnot = "MEILIN_APP5B"
~ appDone = "false"
~ currentEmotion = "neutral"

I could barely make it down the street it was crowded with people celebrating! We’ve gained land and pushed enemy troops away from the city!  /*happy*/

The war isn’t over yet though. The celebration has been a relief, but we still have a long road ahead of us.  We can’t lose focus now. *neutral*/

My volunteers are distracted and have relaxed on their duties. I hope it won’t continue. We are needed now more than ever.  /*nervous*/

We must never forget this victory was made possible through sacrifice. Many people lost loved ones. We need to be there for our community. /*sad*/

I’m on my way to visit a family who lost their father in the battle. I hope there is something I can do to bring some comfort. 



~ nextKnot = "APP5B_Con"

->APP5B_Con

==APP5B_Con==

~ currentKnot = "APP5B_Con"
~ canChoiceBeMade = "yes"

*   Body
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5B_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5B_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5B_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5B_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 5B Result A]
[Intellect: Appointment 5B Result B]
[Tranquility: Appointment 5B Result C]
[Charisma: Appointment 5B Result D]
*/


==MEILIN_APP5B_RESULT_A==

~ currentKnot = "MEILIN_APP5B_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I was able to help our neighbors by completing housework. I wasn’t much, but it provided relief.  You helped me feel recharged after a long day of work. /*neutral*/



->END

==MEILIN_APP5B_RESULT_B==

~ currentKnot = "MEILIN_APP5B_RESULT_B"
~ currentEmotion = "neutral"
~ appDone = "true"

It’s impossible to outsmart grief, no matter how hard any of us tries. It must run its course. /*sad*/

As hard as it is to accept, all I can do is be a comforting presence for our neighbors. 



->END


==MEILIN_APP5B_RESULT_C==

~ currentKnot = "MEILIN_APP5B_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

It’s understandable my volunteers were excited by the good news. You helped me have patience.  /*neutral*/

They took time to celebrate, then joined me to comfort our neighbors. 

We were able to complete household chores and cook dinner for the family.
 


->END

==MEILIN_APP5B_RESULT_D==

~ currentKnot = "MEILIN_APP5B_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I was able to assist our neighbors by keeping the children entertained by telling stories while the mom was able to spend time with her sister. /*neutral*/


->END
