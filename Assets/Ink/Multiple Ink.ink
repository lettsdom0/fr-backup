->MEILIN_INDRA_APP4

VAR currentKnot = "none"
VAR nextKnot = "none"
VAR currentEmotion = "none"
VAR appDone = "false"
VAR resultDone = "false"
VAR canChoiceBeMade = "no"

==MEILIN_INDRA_APP4==

~ currentKnot = "MEILIN_INDRA_APP4"
~ appDone = "false"
~ resultDone = "false"

/* MEI LIN ENTERS */

/* MEI LIN: */
The Homefront Action Movement has had an excellent yield of crops. We’ve been distributing
produce across the city.
Still, it feels like we aren’t moving fast enough. People in the city are starving.
More and more garden space is being donated to grow crops. We have a team educating
people on gardening. But there must be more we can do.

/* INDRA: */
So good to see you Mei Lin! I don’t mean to interrupt, but I have an urgent matter. This will just
take a moment.
I am hosting a dinner party to solidify my position among the senators. They could all use a
night of fun. Spirits have been down with war matters to tend to.
It was abnormally difficult to acquire enough provisions for a five-course meal, the prices have
skyrocketed, but I was able to cope.
Yet, I’m unable to cope with my staff. No matter what I do, no one will agree to work in the
evenings anymore!
How can I inspire them to assist me for this crucial event?

/* MEI LIN: */
They don’t need inspiration! They need to do everything they can to survive.
They need to be with their families before they are taken away. They need to tend to their
gardens or there won’t be anything to eat.
This city has been blocked from the rural territories. Food is no longer able to be shipped in.
Of course, that doesn’t affect those with big houses. You and your friends are the only ones
who can afford what little resources are able to make it into the city.

/* INDRA: */
You think I don’t know people are suffering? You think I don’t feel the weight every night?
Can’t you see I’m trying to put an end these unjust tragedies. With this dinner Zaremir could
finally win influence over the generals and he will put an end to the war.
That reminds me, the floral arrangements, will they be ready?

/* MEI LIN: */
You don’t have to worry. Your order will be completed on time and with the utmost care as
always.

/* INDRA: */
I’m immensely grateful Mei Lin. This dinner could change the course of the war.

/* [Give Potion to Indra] */


/* INDRA LEAVES */

/* MEI LIN: */
Maybe I shouldn’t be so hard on them. I realize I’ve been asking too much of my volunteers
myself. What can I do to ease the pressure?

/* [Give Potion to Mei Lin] */




~ nextKnot = "MEILIN_INDRA_APP4"

~ currentEmotion = "neutral"
->MEILIN_INDRA_APP4_Con

==MEILIN_INDRA_APP4_Con==

~ currentKnot = "APP1_Con"

*   Body

    /* INDRA: */
    I’m sure we are on our way to peace!
    
    /* MEI LIN: */
    Thank you, I couldn’t do this without all your help.
    
    ~ nextKnot = "MEILIN_INDRA_APP4_RESULT_A"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   Intellect

    /* INDRA: */
    I’m sure we are on our way to peace!
    
    /* MEI LIN: */
    Thank you, I couldn’t do this without all your help.
    
    ~ nextKnot = "MEILIN_INDRA_APP4_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   Tranquility

    /* INDRA: */
    I’m sure we are on our way to peace!

    /* MEI LIN: */
    Thank you, I couldn’t do this without all your help.
    
    ~ nextKnot = "MEILIN_INDRA_APP4_RESULT_C"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   Charisma

    /* INDRA: */
    I’m sure we are on our way to peace!

    /* MEI LIN: */
    Thank you, I couldn’t do this without all your help.
    
    ~ nextKnot = "MEILIN_INDRA_APP4_RESULT_D"


    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END

/*
[Body: Appointment 1 Result A]
[Intellect: Appointment 1 Result B]
[Tranquility: Appointment 1 Result C]
[Charisma: Appointment 1 Result D]
*/

==MEILIN_INDRA_APP4_RESULT_A==

~ currentKnot = "MEILIN_INDRA_APP4_RESULT_A"



~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==MEILIN_INDRA_APP4_RESULT_B==

~ currentKnot = "MEILIN_INDRA_APP4_RESULT_B"



~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==MEILIN_INDRA_APP4_RESULT_C==

~ currentKnot = "MEILIN_INDRA_APP4_RESULT_C"



~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==MEILIN_INDRA_APP4_RESULT_D==

~ currentKnot = "MEILIN_INDRA_APP4_RESULT_D"


 
~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==DANTE_POPPY_APP5==

~ currentKnot = "DANTE_POPPY_APP5"
~ appDone = "false"
~ resultDone = "false"

/* [POPPY AND ZINNIA ENTER] */

/* ZINNIA: */
I’m glad to see you’re still open given how extreme the scarcity of supplies has becoming.
If it wasn’t for Mei Lin’s community initiative, this city would have crumbled long before the threat of invasion.
I have something sensitive I’m hoping you can help me with. Please keep this just between you and I. 
It looks like you have another appointment, we’ll resume our conversation once they leave.


/* [DANTE ENTERS] */

/* DANTE: */
You can’t imagine the stress I’m under, so many people are demanding my attention between the Senate and my loyal followers. Everyone has a problem for me to solve.
I hate to tell them that my connection to the fates is… weak, it’s from being surrounded by all the emotional turmoil, I’m sure you’ve been experiencing this yourself. 
Oh, I see you’re with another client. I didn’t mean to intrude, but this is urgent. 


/* POPPY: */
Mom! It’s the Seer from the market! I bet he could tell us how Dad is doing!

/* DANTE: */
Oh, a pair of devoted followers. I know, I know such personal interaction can be overwhelming,
please try to contain yourselves.

/* DANTE: */
Now young lady, tell me how the fates can serve and I will assist you.

/* POPPY: */
My Dad was sent to fight, he used to write us every day, but now the letters have stopped.

/* DANTE: */
Let me contact the fates, this will take only a moment.

/* ZINNIA: */
While they’re distracted, I must confide in you.  City Officials have been coming to the university, I’ve been told they were asking about me. 

It’s not unexpected, but yesterday, one of them showed up at my house. They claimed it was nothing but a wellness check that they were doing for the neighborhood.

I haven’t been able to shake the feeling I’m being followed ever since. 

If anything should happen to me, Poppy will be alone. Please, whatever you can do, please make sure she is safe. 

Do you have anything that could ease my mind? 


/* [ Give Zinnia Potion] */



/* [POPPY AND ZINNIA EXIT] */

/* DANTE: */
I hope their predictions proved to be satisfactory. I don’t need any more people complaining.
A small group are growing concerned, and unfortunately vocal, as more and more of my
predictions are proving to be… less than accurate.
I can’t let my devotees down. Could you help me center myself with the fates once again?

/* [Give Dante Potion] */




~ nextKnot = "DANTE_POPPY_APP5_Con"

~ currentEmotion = "neutral"
->DANTE_POPPY_APP5_Con

==DANTE_POPPY_APP5_Con==

~ currentKnot = "DANTE_POPPY_APP5_Con"

*   Body

    /* ZINNIA: */
    Please, don’t forget what we’ve discussed.
    
    /* DANTE: */
    I believe the fates are saying… we will meet again!
    
    ~ nextKnot = "DANTE_POPPY_APP5_RESULT_A"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   Intellect
    
    /* ZINNIA: */
    Please, don’t forget what we’ve discussed.
    
    /* DANTE: */
    I believe the fates are saying… we will meet again!
    
    ~ nextKnot = "DANTE_POPPY_APP5_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   Tranquility

    /* ZINNIA: */
    Please, don’t forget what we’ve discussed.
    
    /* DANTE: */
    I believe the fates are saying… we will meet again!
    
    ~ nextKnot = "DANTE_POPPY_APP5_RESULT_C"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   Charisma

    /* ZINNIA: */
    Please, don’t forget what we’ve discussed.
    
    /* DANTE: */
    I believe the fates are saying… we will meet again!
    
    ~ nextKnot = "DANTE_POPPY_APP5_RESULT_D"


    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END

/*
[Body: Appointment 1 Result A]
[Intellect: Appointment 1 Result B]
[Tranquility: Appointment 1 Result C]
[Charisma: Appointment 1 Result D]
*/





==DANTE_POPPY_APP5_RESULT_A==

~ currentKnot = "DANTE_POPPY_APP5_RESULT_A"

    /* ZINNIA: */

Zinnia: Things haven’t changed since I last visited. I feel eyes on me everywhere I go.
I’ve started staying up long into the night, constantly watching the street. 

Luckily, I’ve had the stamina to sustain the long hours and still tend to my work the next day.                               

I don’t think Poppy has noticed yet.

Poppy: What haven’t I noticed? Is there a surprise?


    /* DANTE: */

Hello, friend. I am... struggling, in my current post. The Senate asked for my input concerning battle strategies.  
My mind was... blank. I lied to them. I told them it would be a glorious success, a turning point in the war-- all the hyperbole I could think of.
We had a victory, but it’s not without casualties. Casualties which I had a hand in. I'm hounded by constant visions of battle. I haven’t had a moment's rest even with your help.  


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==DANTE_POPPY_APP5_RESULT_B==

~ currentKnot = "DANTE_POPPY_APP5_RESULT_B"

    /* ZINNIA: */

Zinnia: Things haven’t changed since I last visited. I feel eyes on me everywhere I go.

I’ve become socially withdrawn. I can’t stop thinking that any of my friends could be monitoring my movements. Everyday I feel like I could be taken away.”

This paranoia continues to eat at me while my mind continues to analyze the motive of those around me. I just hope Poppy hasn’t noticed.

Poppy: What haven’t I noticed? Is there a surprise?


    /* DANTE: */

Hello, friend. I am... struggling, in my current post. The Senate asked for my input concerning battle strategies.  
My mind was... blank. I lied to them. I told them it would be a glorious success, a turning point in the war-- all the hyperbole I could think of.
We had a victory, but it’s not without casualties. Casualties which I had a hand in. I'm hounded by constant visions of battle. Even with your help I can’t rationalize my role in these events.   


~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==DANTE_POPPY_APP5_RESULT_C==

~ currentKnot = "DANTE_POPPY_APP5_RESULT_C"

    /* ZINNIA: */

Zinnia: Things haven’t changed since I last visited. I feel eyes on me everywhere I go.

I wait and wait, but no one has approached me. I never thought I could be so patient, not when my family is at stake.

I shouldn’t say too much. I don’t know who is listening.

Poppy: It will be okay Mom! 


    /* DANTE: */

Hello friend. I am... struggling, in my current post. The Senate asked for my input concerning battle strategies.  
My mind was... blank. I lied to them. I told them it would be a glorious success, a turning point in the war-- all the hyperbole I could think of.
We had a victory, but it’s not without casualties. Casualties which I had a hand in. I'm hounded by constant visions of battle. I haven’t had a moment's rest even with your help.  


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==DANTE_POPPY_APP5_RESULT_D==

~ currentKnot = "DANTE_POPPY_APP5_RESULT_D"

    /* ZINNIA: */
    
Zinnia: Things haven’t changed since I last visited. I feel eyes on me everywhere I go.
    
School has become tense. I was asked to volunteer at a special university wide assembly to support the war effort.

I convinced another faculty member to take my place. She hasn’t been seen since.

I shouldn’t say too much. I don’t know who is listening.

Poppy: It will be okay Mom!


    /* DANTE: */

Hello, friend. I am... struggling, in my current post. The Senate asked for my input concerning battle strategies.  
My mind was... blank. I lied to them. I told them it would be a glorious success, a turning point in the war-- all the hyperbole I could think of.
We had a victory, but it’s not without casualties. Casualties which I had a hand in. I'm hounded by constant visions of battle. Even with your help I can’t talk my way out of this quandary. 

 
~ currentEmotion = "neutral"
~ resultDone = "true"
->END