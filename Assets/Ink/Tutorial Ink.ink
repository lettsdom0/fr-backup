->TUTORIAL_PT_1

VAR currentKnot = "none"
VAR nextKnot = "none"
VAR canChoiceBeMade = "no"
VAR tutorialDone = "false"

==TUTORIAL_PT_1==

~ currentKnot = "TUTORIAL_PT_1"
"Alright, that seems to be the last box. Welcome to Echevaria! By the way, I’m curious about your craft. I’ve met a few prophets and fortune tellers before, but I’ve never met an apothecary."
Everyone has personal attributes that predict their patterns of behavior. Apothecaries use potions to affect those attributes.
One’s attributes influence their ability to handle the demands of daily life; your immediate choices will affect the immediate future, but the culmination of your choices will decide their fate.

"Hey, one of our crew hurt his back pretty bad lifting something. Do you have anything that can help him out?"
 You unpack some ingredients out of a pile of boxes and concoct a potion with the first few things you find.
"Is it ready?"

~ canChoiceBeMade = "yes"


->TUTORIAL_PT_2

==TUTORIAL_PT_2==
~ currentKnot = "TUTORIAL_PT_2"
*Body
    ~ canChoiceBeMade = "no"
    You are able to brew up the potion he needs. 
    You have used up the last of your magical energy, but it will regenerate on your rest day tomorrow. Each week you must rest one day to restore your magical energy for the week.
    "Thanks! We’ll give this to him!"
    "We've finished moving everything in, we hope you enjoy your time here in Echevaria!"
    ~ tutorialDone = "true"
    ->END