->POPPY_APP1_INTRO

VAR currentKnot = "none"
VAR nextKnot = "none"
VAR currentEmotion = "none"
VAR appDone = "false"
VAR resultDone = "false"
VAR canChoiceBeMade = "no"
VAR weekDone = "false"

==POPPY_APP1_INTRO==

~ currentKnot = "POPPY_APP1_INTRO"
~ resultDone = "false"
~ currentEmotion = "none"
~ appDone = "false"


Zinnia: Good morning! It’s nice to meet you! My name is Zinnia and this is my daughter Poppy.

Zinnia: I’m a professor up at the university, and Poppy just started her the third year of school. We were on our way in, but I wanted to stop by to welcome you to the neighborhood!

~ currentEmotion = "happy_neutral"
I’ve been having some problems lately and one of the other professors referred me to you.

Poppy: Do you have potions to turn me invisible? Can you make me taller? 
~ currentEmotion = "sad_neutral"
Zinnia: Poppy, I don’t think that’s their main focus. We’re here to help with my headaches.

Poppy: Yeah, Mom has really bad headaches and then she doesn’t want to play. 

Zinnia: I just can’t seem to build up my energy. I’ve still managed to teach all my lessons for the day, but I’m worn out by the time that I get home with Poppy.
~ currentEmotion = "happy_neutral"
Even after a long day, she is still so bright and full of energy.

Poppy: Have you ever turned someone into a frog?

Zinnia: My husband, Rowan, works in the evening, he helps as much as he can, but he has to leave after we all have dinner together.

I’m trying my best to keep up with Poppy at home, but it has become a challenge for me.
Do you have anything to recommend? 




~ nextKnot = "APP1_Con"

->APP1_Con

==APP1_Con==

~ currentKnot = "APP1_Con"
~ canChoiceBeMade = "yes"


*   Body
    ~ currentEmotion = "neutral"
    Zinnia: Just knowing you’re here to help has already provided some relief. Thank you. /*neutral*/
    
    Poppy: Bye!
 
    ~ nextKnot = "POPPY_APP1_RESULT_A"
    

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    Zinnia: Just knowing you’re here to help has already provided some relief. Thank you. /*neutral*/
    
    Poppy: Bye!
    
    ~ nextKnot = "POPPY_APP1_RESULT_B"
    
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    ~ currentEmotion = "neutral"
    Zinnia: Just knowing you’re here to help has already provided some relief. Thank you. /*neutral*/
    
    Poppy: Bye!
    
    ~ nextKnot = "POPPY_APP1_RESULT_C"
    
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    ~ currentEmotion = "neutral"
    Zinnia: Just knowing you’re here to help has already provided some relief. Thank you. /*neutral*/
    
    Poppy: Bye!
    
    ~ nextKnot = "POPPY_APP1_RESULT_D"
    
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 1 Result A]
[Intellect: Appointment 1 Result B]
[Tranquility: Appointment 1 Result C]
[Charisma: Appointment 1 Result D]
*/



==POPPY_APP1_RESULT_A==

~ currentKnot = "POPPY_APP1_RESULT_A"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP2"
~ appDone = "true"

Zinnia: Your services have been such a relief. Since I took your potion, I’ve been feeling stronger, my energy is up, and my headaches have gone away.

Zinnia: I’ve been able to keep Poppy entertained in the evenings and help her with her schoolwork.

Zinnia: The other professors will be glad to know your potions are so efficient.

Poppy: Thanks for making my mom feel better! I’m glad we’re friends now! 


~ resultDone = "true"

->END

==POPPY_APP1_RESULT_B==

~ currentKnot = "POPPY_APP1_RESULT_B"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP2"
~ weekDone = "false"
~ appDone = "true"

Zinnia: Thanks to your recommendation, I’ve finally been able to organize my thoughts, something I just haven’t had the energy to do. 

With this boost of intellect, I had a spark of creativity and created a series of logic puzzles to keep Poppy busy in the evening.
~ currentEmotion = "happy_neutral"
It was just the solution I needed to keep her occupied.  Poppy is determined she won’t be outmatched

Poppy: I’m the best in my class at puzzles! 

Zinnia: I’ve been able to take some time to relax and gain back my energy. Thanks again. 


~ resultDone = "true"

->END


==POPPY_APP1_RESULT_C==

~ currentKnot = "POPPY_APP1_RESULT_C"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP2"
~ appDone = "true"

Zinnia: Your potion has brought me some relief. Since my last visit, I’ve been able to give my mind some rest.

It has helped me feel more present when I’m at home with Poppy, and I’ve slowly gained back some energy. 

Poppy: Thanks for trying to help my mom! 

Zinnia: Unfortunately, no matter what I do I still haven’t been able to get rid of these unrelenting headaches. Thanks for trying. 


~ resultDone = "true"

->END

==POPPY_APP1_RESULT_D==

~ currentKnot = "POPPY_APP1_RESULT_D"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP2"
~ appDone = "true"

Zinnia: I encouraged Poppy  to do some quiet activities in the evening after she has finished her homework. She’s so creative and enjoys drawing. 

She’s even bargained for a bouquet from the flower shop if she can keep it up for a whole week. 

Poppy: Would you like a drawing? I can make you one!

Zinnia: She loves a challenge! I’m just grateful to be able to get some downtime at the end of the day. Thank you!



~ resultDone = "true"
 
->END

==POPPY_APP2==

~ currentKnot = "POPPY_APP2"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "neutral"



Zinnia: Good morning! I’m so grateful for all the help you’ve given my family.
Poppy and I are on our way to school, and I wanted to stop by.
~ currentEmotion = "sad_upset"
Poppy has been having a difficult time focusing in class. She’s normally enthusiastic about her schoolwork and never hesitates to help her classmates.

But l was told she has been socializing too much in class, and distracting others from their work, there are even reports she has been falling asleep in class.


Poppy: I didn’t mean to!  
~ currentEmotion = "sad_neutral"

Zinnia: She’s so personable, and doesn’t have anyone else at home her age to play with.
 I think she gets excited at school to be with the other kids, and wears herself out at recess.
Do you have anything that could help her be more attentive? 



~ nextKnot = "APP2_Con"

->APP2_Con

==APP2_Con==

~ currentKnot = "APP2_Con"
~ canChoiceBeMade = "yes"

*   Body
    ~ currentEmotion = "neutral"
    Thank you! I let you know how it goes. /*neutral*/
 
    ~ nextKnot = "POPPY_APP2_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    ~ currentEmotion = "neutral"
    Thank you! I let you know how it goes. /*neutral*/
    
    ~ nextKnot = "POPPY_APP2_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    ~ currentEmotion = "neutral"
    Thank you! I let you know how it goes. /*neutral*/
    
    ~ nextKnot = "POPPY_APP2_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    ~ currentEmotion = "neutral"
    Thank you! I let you know how it goes. /*neutral*/
    
    ~ nextKnot = "POPPY_APP2_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 2 Result A]
[Intellect: Appointment 2 Result B]
[Tranquility: Appointment 2 Result C]
[Charisma: Appointment 2 Result D]
*/


==POPPY_APP2_RESULT_A==

~ currentKnot = "POPPY_APP2_RESULT_A"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP3"
~ appDone = "true"

Zinnia: Your potion is working well! Poppy has more energy and is no longer falling asleep in class. 

In fact, it's given her so much energy she is having trouble staying in her seat and calming down in the evenings at home.

Poppy: Do you like playing games? How many potions do you have in your shop? 

Zinnia: Poppy, we’ve got to go, we’ll come back another time.  Thank you again, you’re a great help. 


~ resultDone = "true"

->END

==POPPY_APP2_RESULT_B==

~ currentKnot = "POPPY_APP2_RESULT_B"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP3"
~ appDone = "true"

Zinnia: Your potion has been working well! Poppy has been able to catch up on all the concepts she missed in class. 

Her comprehension is so good, she has been able to assist any students that need help.

Poppy: I love helping my friends! 

Zinnia: Her teacher has been so impressed with her work! Thanks for your help.
 /*neutral*/

~ resultDone = "true"

->END


==POPPY_APP2_RESULT_C==

~ currentKnot = "POPPY_APP2_RESULT_C"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP3"
~ appDone = "true"

Zinnia: Your potion seems to be helping. Poppy has improved, her mind has been calmed and it is easier for her to pay attention. 

Unfortunately, she is still falling asleep in class and missing important concepts. 

Poppy: Sometimes school is so boring… But thanks for helping. 
 /*frustrated_upset*/

~ resultDone = "true"

->END

==POPPY_APP2_RESULT_D==

~ currentKnot = "POPPY_APP2_RESULT_D"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP3"
~ appDone = "true"

Zinnia: Hello, I hope you’re doing well. Poppy is spending more time talking, especially with the teachers. 

She’s been convincing them that her socialization amongst her classmates is good for classroom morale. 

Thankfully her teacher has agreed to work with her after class to make sure she is comprehending everything she needs to. 

Poppy: Now I have more school…

Zinnia: Thanks again for your potions. I know you do good work.  

~ resultDone = "true"
 
->END



==POPPY_APP3A==
~ appDone = "false"
~ canChoiceBeMade = "no"
~ currentKnot = "POPPY_APP3A"
~ resultDone = "false"
~ currentEmotion = "neutral"



Zinnia: Good morning, I’m glad to see you’re still well.

The neighborhood has changed so much since war broke out. I barely recognize it. I’m scared to let Poppy wander on her own now. 

The fighting is miles away, but more and more students are considering leaving the city with their families and my colleagues and I are worried we won’t have jobs soon.

We’ve lived in peace for so long, I hoped my daughter would never see such conflict. I should know better I guess. I am a scholar of history after all.

Poppy: Can’t things just be normal again? I miss walking to school with my friends. 

Zinnia: We just have to continue to be strong. Everything is shifting, now there is division amongst the faculty. I don’t support this war, and I’m not shy about voicing my opinions. 

My husband is concerned about our safety if I continue to talk about my stance on the war publicly, and I understand his concerns. 

But I can’t let my child and my students think that I’d stand by idly and watch a war tear them from their homes and families. 

I need to keep my family together. Do you have anything that can help?


~ nextKnot = "APP3A_Con"
~ appDone = "false"

->APP3A_Con

==APP3A_Con==

~ currentKnot = "APP3A_Con"
~ canChoiceBeMade = "yes"

*   Body

    ~ currentEmotion = "neutral"

    Zinnia: Thank you, I hope we’ll see each other again. /*neutral*/
 
    ~ nextKnot = "POPPY_APP3A_RESULT_A"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect

    ~ currentEmotion = "neutral"

    Zinnia: Thank you, I hope we’ll see each other again. /*neutral*/
    
    ~ nextKnot = "POPPY_APP3A_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility

    ~ currentEmotion = "neutral"

    Zinnia: Thank you, I hope we’ll see each other again. /*neutral*/
    
    ~ nextKnot = "POPPY_APP3A_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma

    ~ currentEmotion = "neutral"
    
    Zinnia: Thank you, I hope we’ll see each other again. /*neutral*/
    
    ~ nextKnot = "POPPY_APP3A_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 3A Result A]
[Intellect: Appointment 3A Result B]
[Tranquility: Appointment 3A Result C]
[Charisma: Appointment 3A Result D]
*/


==POPPY_APP3A_RESULT_A==

~ currentKnot = "POPPY_APP3A_RESULT_A"
~ currentEmotion = "frustrated_upset"
~ appDone = "true"
~ nextKnot = "POPPY_APP4"

Zinnia: The stress has been keeping me awake all night. At least I’m lucky my family is still together, but I can’t stop worrying about the future. /*frustrated_upset*/

    ~ currentEmotion = "neutral"

Thankfully your potion has helped me through the day after sleepless nights. I don’t think I’d have the energy to get out of bed without it. /*neutral*/



->END

==POPPY_APP3A_RESULT_B==

~ currentKnot = "POPPY_APP3A_RESULT_B"
~ currentEmotion = "sad_upset"
~ nextKnot = "POPPY_APP4"
~ appDone = "true"

Despite my efforts, the faculty still refuse to stand up for their students. They’re terrified of losing their jobs.  /*sad_upset*/

I worry that even if their own children are harmed they will say nothing out of fear. 

I’m not like them, I will protect Poppy and my students however I can. 

->END


==POPPY_APP3A_RESULT_C==

~ currentKnot = "POPPY_APP3A_RESULT_C"
~ currentEmotion = "sad_upset"
~ nextKnot = "POPPY_APP4"
~ appDone = "true"

Nothing has changed amongst the faculty I’m afraid. We’ve continued to debate, and I won’t betray my convictions to make others more comfortable. /////*sad_upset*/

If I didn’t have my supporters within the school, I think I would’ve been reported for treason. I’m not sure who to trust anymore.


->END

==POPPY_APP3A_RESULT_D==

~ currentKnot = "POPPY_APP3A_RESULT_D"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP4"
~ appDone = "true"

There was a faculty meeting . I was able to plead my case. We need to stick together in order to make it through this ordeal. /*neutral*/

Community is important now more than ever. I explained I’d do anything to support our families, but I won’t support the Senate. 

Others were able to understand my side of things and the division has subsided, for now.

->END



==POPPY_APP3B==
~ appDone = "false"
~ canChoiceBeMade = "no"
~ currentKnot = "POPPY_APP3B"

~ resultDone = "false"


Zinnia: Good morning, I’m glad to see you’re still well. /*neutral*/

~ currentEmotion = "sad_upset"

The neighborhood has changed so much since war broke out. I barely recognize it. /*sad_upset*/

 The fighting is miles away, but parents at the school are considering leaving the city and the teachers are worried we won’t have jobs soon.
 
We’ve lived in peace for so long, I hoped my daughter would never see such conflict. I should know better I guess, I’m a scholar of history after all.

Poppy: Can’t things just be normal again? I miss walking to school with my friends. 

Zinnia: We just have to continue to be strong.Everything is shifting, now there is division amongst the faculty. I don’t support this war, and I’m not shy about voicing my opinions.

There are others who support me, but some are worried I’ll draw negative attention to the university, but I can’t stay silent. Not when families are being torn apart.

This war is nothing but a dispute of egos at the expense of innocent people.

I see my former students, barely adults themselves, shipped off to battle. Then it’s left to us to console their parents and comfort their siblings when they don’t come home.

If history proves anything, it’s just going to get worse. 

I need to keep my family together. Do you have anything that can help?


~ nextKnot = "APP3BA_Con"

->APP3B_Con

==APP3B_Con==

~ currentKnot = "APP3B_Con"
~ canChoiceBeMade = "yes"

*   Body
    Thank you, I hope we’ll see each other again. /*neutral*/ /*neutral*/
 
    ~ nextKnot = "POPPY_APP3B_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    Thank you, I hope we’ll see each other again. /*neutral*/
    
    ~ nextKnot = "POPPY_APP3B_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    Thank you, I hope we’ll see each other again. /*neutral*/
    
    ~ nextKnot = "POPPY_APP3B_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    Thank you, I hope we’ll see each other again. /*neutral*/
    
    ~ nextKnot = "POPPY_APP3B_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 3B Result A]
[Intellect: Appointment 3B Result B]
[Tranquility: Appointment 3B Result C]
[Charisma: Appointment 3B Result D]
*/


==POPPY_APP3B_RESULT_A==

~ currentKnot = "POPPY_APP3B_RESULT_A"
~ currentEmotion = "frustrated_upset"
~ nextKnot = "POPPY_APP4"
~ appDone = "true"

Zinnia: The stress has been keeping me awake all night. At least I’m lucky my family is still together, but I can’t stop worrying about the future. /*frustrated_upset*/

~ currentEmotion = "neutral"

Thankfully your potion has helped me through the day after sleepless nights. I don’t think I’d have the energy to get out of bed without it. /*neutral*/


->END

==POPPY_APP3B_RESULT_B==

~ currentKnot = "POPPY_APP3B_RESULT_B"
~ currentEmotion = "sad_upset"
~ nextKnot = "POPPY_APP4"
~ appDone = "true"

Zinnia: Despite my efforts, the rift between the faculty hasn’t improved./*sad_upset*/

I only seem to isolate myself further. I try to explain my reasoning, but they claim I come off as unsupportive and compassionless for those involved in the war.


->END


==POPPY_APP3B_RESULT_C==

~ currentKnot = "POPPY_APP3B_RESULT_C"
~ currentEmotion = "sad_upset"
~ nextKnot = "POPPY_APP4"
~ appDone = "true"

Zinnia: Nothing has changed amongst the faculty I’m afraid. We’ve continued to debate, and I won’t betray my convictions to make other more comfortable. /*sad_upset*/

If I didn’t have my supporters within the school, I think I would’ve been reported for treason. I’m not sure who to trust anymore.

->END

==POPPY_APP3B_RESULT_D==

~ currentKnot = "POPPY_APP3B_RESULT_D"
~ currentEmotion = "neutral"
~ nextKnot = "POPPY_APP4"
~ appDone = "true"

Zinnia: There was a faculty meeting today. I was able to plead my case, the teachers need to stick together in order to make it through this ordeal./*neutral*/

Community is important now more than ever. I explained I’d do anything to support our families, but I won’t support the Senate. 

Others were able to understand my side of things and the division has subsided.


->END




==POPPY_APP4A==
~ resultDone = "false"
~ canChoiceBeMade = "no"
~ currentKnot = "POPPY_APP4A"
~ appDone = "false"
~ currentEmotion = "frustrated_upset"


Zinnia: Have you been able to get a hold of enough supplies for your potions?



Fresh food is becoming harder and harder to find. I just waited in line for hours at the market, only to be turned away.
 /*frustrated_upset*/

Poppy: My feet hurt. When are we going to go home? 

Zinnia: Just after we get a potion. This is our last stop, then we’ll go home.

Most of the food production has been requisitioned by the Senate to feed the army. My family has some reserves, but not enough to keep us much longer.

I know other families are having the same problem, that line was filled with families just like ours. 

 It’s hard to see how we’ll make it through. The Senate doesn’t seem to care. They should be taking responsibility for the wellbeing of their citizens.
 
Poppy: Can’t you use your potions to make everyone get along again? 

Zinnia: Everyone is doing what they can Poppy. I’ve been occasionally volunteering to help bring surplus supplies to people who need them, but I only have so much time. 

We need to bring together everyone in the community to tell the Senate to end this war. All of us are suffering and we need to make our voices heard. 

My family is about to go through even harder times. 

~ currentEmotion = "sad_upset"

My husband has received his draft notice. He must report to training tomorrow. 

I need something to help us through all of this. 



~ nextKnot = "APP4A_Con"

->APP4A_Con



==APP4A_Con==

~ currentKnot = "APP4A_Con"
~ canChoiceBeMade = "yes"

*   Body
    Hopefully this will provide some kind of comfort. /*sad_upset*/
 
    ~ nextKnot = "POPPY_APP4A_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    Hopefully this will provide some kind of comfort. /*sad_upset*/
    
    ~ nextKnot = "POPPY_APP4A_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    Hopefully this will provide some kind of comfort. /*sad_upset*/
    
    ~ nextKnot = "POPPY_APP4A_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    Hopefully this will provide some kind of comfort. /*sad_upset*/
    
    ~ nextKnot = "POPPY_APP4A_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 4A Result A]
[Intellect: Appointment 4A Result B]
[Tranquility: Appointment 4A Result C]
[Charisma: Appointment 4A Result D]
*/


==POPPY_APP4A_RESULT_A==

~ currentKnot = "POPPY_APP4A_RESULT_A"
~ currentEmotion = "frustrated_upset"
~ appDone = "true"

Zinnia: I’ve found that I'm able to sustain myself on smaller meals without sacrificing my body’s strength. I’ve been able to prioritize Poppy and ensure she gets her full share.

Our reserves were running low, and I’m not sure how much longer this potion would’ve lasted, but thankfully the community has organized a farming initiative.


->END

==POPPY_APP4A_RESULT_B==

~ currentKnot = "POPPY_APP4A_RESULT_B"
~ currentEmotion = "neutral"
~ appDone = "true"

 Zinnia: Mei Lin approached me about her farming initiative. It will provide the whole neighborhood with food. I agreed to use my garden to contribute to the project.
 
Poppy: I get to help out too!

Zinnia: I was even able to offer my services to organize a way for the initiative to provide supplies to the school for lunches.



->END


==POPPY_APP4A_RESULT_C==

~ currentKnot = "POPPY_APP4A_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

Zinnia: The potion has relaxed my mind at night and has helped me sleep, but it’s been hard to find energy throughout the day, by dinner the headaches start again.

The only thing keeping me going is taking care of Poppy. I must keep fighting, in some way I must help bring peace again for her.



->END

==POPPY_APP4A_RESULT_D==

~ currentKnot = "POPPY_APP4A_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

Zinnia: A former student of mine works at the Generals’ office. I was able to persuade them to keep me informed of all the troop movements.”

~ currentEmotion = "sad_upset"

There isn’t a moment when I’m not worrying about my husband, but it helps to know he is still safe within city limits, at least for now. Things are developing rapidly.

Poppy: I hope Dad can come home soon. 



->END



==POPPY_APP4B==

~ currentKnot = "POPPY_APP4B"
~ appDone = "false"
~ resultDone = "false"
~ canChoiceBeMade = "no"

~ currentEmotion = "frustrated_upset"


Zinnia: Have you been able to get a hold of enough supplies for your potions?



Fresh food is becoming harder and harder to find. I just waited in line for hours at the market, only to be turned away.
 /*frustrated_upset*/

Poppy: My feet hurt. When are we going to go home? 

Zinnia: Just after we get a potion. This is our last stop, then we’ll go home.

Most of the food production has been requisitioned by the Senate to feed the army. My family has some reserves, but not enough to keep us much longer.

I know other families are having the same problem, that line was filled with families just like ours. 

 It’s hard to see how we’ll make it through. The Senate doesn’t seem to care. They should be taking responsibility for the wellbeing of their citizens.
 
Poppy: Can’t you use your potions to make everyone get along again? 

Zinnia: Everyone is doing what they can Poppy. 

 It’s hard to see how we’ll make it through. The Senate doesn’t seem to care. They should be taking responsibility for the well-being of their citizens.
 
Yet, all we hear is ‘Don’t worry, it will all be over by the solstice,’ while they choose to send our families into battle.  They choose to destroy generations.

They still get to smile and throw parties. They give speeches on nobility and sacrifice, but they never offer up sacrifice from themselves.

They despair over their failed strategies; they don’t despair over the loss of our lives.

Now my family has become another statistic, more lives the Senate has torn apart.

~ currentEmotion = "sad_upset"

My husband has received his draft notice. He must report to training tomorrow. 

I need something to help us through all of this. 


~ nextKnot = "APP4BA_Con"

->APP4B_Con

==APP4B_Con==

~ currentKnot = "APP4B_Con"
~ canChoiceBeMade = "yes"

*   Body
    Hopefully this will provide some kind of comfort. /*neutral*/
 
    ~ nextKnot = "POPPY_APP4B_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect
    Hopefully this will provide some kind of comfort. /*neutral*/
    
    ~ nextKnot = "POPPY_APP4B_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility
    Hopefully this will provide some kind of comfort. /*neutral*/
    
    ~ nextKnot = "POPPY_APP4B_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Charisma
    Hopefully this will provide some kind of comfort. /*neutral*/
    
    ~ nextKnot = "POPPY_APP4B_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[Body: Appointment 4B Result A]
[Intellect: Appointment 4B Result B]
[Tranquility: Appointment 4B Result C]
[Charisma: Appointment 4B Result D]
*/


==POPPY_APP4B_RESULT_A==

~ currentKnot = "POPPY_APP4B_RESULT_A"
~ currentEmotion = "neutral"

Zinnia: I’ve found that I'm able to sustain myself on smaller meals without sacrificing my body’s strength. I’ve been able to prioritize Poppy and ensure she gets her full share.

Our reserves were running low, and I’m not sure how much longer this potion would’ve lasted, but thankfully the community has organized a farming initiative.



->END

==POPPY_APP4B_RESULT_B==

~ currentKnot = "POPPY_APP4B_RESULT_B"
~ currentEmotion = "neutral"
~ appDone = "true"

 Zinnia: Mei Lin approached me about her farming initiative. It will provide the whole neighborhood with food. I agreed to use my garden to contribute to the project.
 
Poppy: I get to help out too!

Zinnia: I was even able to offer my services to organize a way for the initiative to provide supplies to the school for lunches.



->END


==POPPY_APP4B_RESULT_C==

~ currentKnot = "POPPY_APP4B_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

Zinnia: The potion has relaxed my mind at night and has helped me sleep, but it’s been hard to find energy throughout the day, by dinner the headaches start again.

The only thing keeping me going is taking care of Poppy. I must keep fighting, in some way I must help bring peace again for her.



->END

==POPPY_APP4B_RESULT_D==

~ currentKnot = "POPPY_APP4B_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

Zinnia: A former student of mine works at the Generals’ office. I was able to persuade them to keep me informed of all the troop movements.”

There isn’t a moment when I’m not worrying about my husband, but it helps to know he is still safe within city limits, at least for now. Things are developing rapidly.

Poppy: I hope Dad can come home soon. 


->END

/* Appointment 5 has multiple people*/ 