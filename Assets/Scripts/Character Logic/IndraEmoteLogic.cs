﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IndraEmoteLogic : MonoBehaviour
{
    //This script deals with indra exlusive emotions and changing of said emotions
    //character emotions exclusive to Indra
    
    public Sprite indra_Boastful;
    public Sprite indra_Frightened;
    public Sprite indra_Shellshocked;
    public Sprite indra_Surprised;
    Actor actor;

    public Image characterEmotion;
    public void Start()
    {
        if (actor == null)
        {
            actor = this.gameObject.GetComponent<Actor>();
        }
        if (characterEmotion == null)
        {
            characterEmotion = GetComponent<Image>();
        }
        actor.setActorSelfImage();

    }
    
    public void changeIndraToBoastful()
    {
        var changingEmote = characterEmotion;
        //changes dante's emotion to boastful
        changingEmote.sprite = indra_Boastful;
    }
    
    public void changeIndraToFrightened()
    {
        var changingEmote = characterEmotion;
        //changes dante's emotion to boastful
        changingEmote.sprite = indra_Frightened;
    }
    public void changeIndraToShellshocked()
    {
        var changingEmote = characterEmotion;
        //changes dante's emotion to injured
        changingEmote.sprite = indra_Shellshocked;

    }
    public void changeIndraToSurprised()
    {
        var changingEmote = characterEmotion;
        //changes dante's emotion to nervous
        changingEmote.sprite = indra_Surprised;
    }
}
