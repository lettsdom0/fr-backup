﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoppyEmoteLogic : MonoBehaviour
{

    //character emotions exclusive to Poppy
    public Sprite only_poppy_PTSD;
    public Sprite only_poppy_Numb;

    //zinnia emotions (easier to do this than just having them be seperate)
    public Sprite only_zinnia_Sick;
    public Sprite only_zinnia_Upset;

    //zinnia and poppy emotions (easier to do this than just having them be seperate)
    public Sprite poppy_Frustrated_zinnia_Neutral;
    public Sprite poppy_Frustrated_zinnia_Upset;
    public Sprite poppy_Happy_zinnia_Neutral;
    public Sprite poppy_Happy_zinnia_Upset;
    public Sprite poppy_Sad_zinnia_Neutral;
    public Sprite poppy_Sad_zinnia_Upset;
    public Sprite poppy_Neutral_zinnia_Upset;
    Actor actor;

    public Image characterEmotion;
    public void Start()
    {
        if (actor == null)
        {
            actor = this.gameObject.GetComponent<Actor>();
        }
        if (characterEmotion == null)
        {
            characterEmotion = GetComponent<Image>();
        }
        actor.setActorSelfImage();
    }
    //poppy exclusive emotions
    public void changeOnlyPoppyToNumb()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = only_poppy_Numb;
    }
    public void changeOnlyPoppyToPTSD()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = only_poppy_PTSD;
    }
    //zinnia exclusive emotions
    public void changeOnlyZinniaToSick()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = only_zinnia_Sick;
    }
    public void changeOnlyZinniaToUpset()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = only_zinnia_Upset;
    }
    //poppy and zinnia together
    public void Poppy_Frustrated_Zinnia_Neutral()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = poppy_Frustrated_zinnia_Neutral;
    }
    public void Poppy_Happy_Zinnia_Neutral()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = poppy_Happy_zinnia_Neutral;
    }
    public void Poppy_Sad_Zinnia_Neutral()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = poppy_Sad_zinnia_Neutral;
    }
    public void Poppy_Frustrated_Zinnia_Upset()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = poppy_Frustrated_zinnia_Upset;
    }
    public void Poppy_Happy_Zinnia_Upset()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = poppy_Happy_zinnia_Upset;
    }
    public void Poppy_Neutral_Zinnia_Upset()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = poppy_Neutral_zinnia_Upset;
    }
    public void Poppy_Sad_Zinnia_Upset()
    {
        Sprite changingEmote = characterEmotion.sprite;
        changingEmote = poppy_Sad_zinnia_Upset;
    }
}
