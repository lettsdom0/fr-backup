﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MeiLinEmoteLogic : MonoBehaviour
{
    //character emotions exclusive to Mei Lin
    public Sprite mei_lin_Nervous;

    Actor actor;

    public Image characterEmotion;
    public void Start()
    {
        if (actor == null)
        {
            actor = this.gameObject.GetComponent<Actor>();
        }
        if (characterEmotion == null)
        {
            characterEmotion = GetComponent<Image>();
        }
        actor.setActorSelfImage();
    }
    public void changeMeiLinToNervous()
    {
        var changingEmote = characterEmotion;
        changingEmote.sprite = mei_lin_Nervous;
    }
}
