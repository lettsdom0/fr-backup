﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour {
    // audio clips and sources
    public AudioClip SceneAudio;
    public AudioClip SceneSFX;
    public AudioClip SceneMusic;
    public AudioClip ScenePSFX;
    public AudioSource SceneMusicSource;
    public AudioSource SceneAudioSource;
    public AudioSource SceneSFXSource;
    public AudioSource ScenePSFXSource;

    //object variables
    private static AudioClip instance;


    public void PlayMe()
    {
        SceneMusicSource.loop = true;
        SceneMusicSource.Play();
    }

    // Use this for initialization
    void Start () {
        SceneMusicSource.clip = SceneMusic;
        SceneAudioSource.clip = SceneAudio;
        SceneSFXSource.clip = SceneSFX;
        ScenePSFXSource.clip = ScenePSFX;
        SceneSFXSource.Play();
        PlayMe();
        //set flowchart variable
        

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            SceneAudioSource.Play();
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            SceneAudioSource.Play();

    }

   public void playOnPotionGiven()
    {
        ScenePSFXSource.Play();
    }
   
    

        void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
            else
            {
                instance = this.SceneAudio;
            }
            DontDestroyOnLoad(this.gameObject);
        }


    void OnApplicationQuit()
    {
      instance = null;
    }
    
}
