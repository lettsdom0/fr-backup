﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMenu : MonoBehaviour
{
    //script handles going back to the menu of fateful remedies.
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            backToMenu();
        }
    }

    void backToMenu()
    {
        //on button press, this script's only function is to
        //send the player back to the main menu.
        SceneManager.LoadScene(0);

    }
}
