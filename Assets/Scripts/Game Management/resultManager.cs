﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resultManager : MonoBehaviour
{
    //script handles parsing of result script.
    /// <summary>
    //how much the result dialogue changes depends on each customer's stats
    //
    /// </summary> //object reference
    public GameObject controller;
    //script references
    public DialogueHandler dialogue;

    InkKnotCheck inkKnotCheck;
    public string fullResultString;
    public string danteResultString;
    public string indraResultString;
    public string meilinResultString;
    public string poppyResultString;



    void Start()
    {
        if (controller == null)
        {
            controller = GameObject.FindGameObjectWithTag("GameController");
            if(inkKnotCheck == null)
            {
                inkKnotCheck = controller.GetComponent<InkKnotCheck>();
            }
            if (dialogue == null)
            {
                dialogue = controller.GetComponent<DialogueHandler>();
            }
        }
        fullResultString = "";
        danteResultString = "";
        indraResultString = "";
        meilinResultString = "";
        poppyResultString = "";
    }


    // Update is called once per frame
    void Update()
    {
       
    }

    public void fillDanteResults()
    {
        //function checks where dante is going next during the reults screen
        //dante reults get filled depending on what subappointment is next
        if(dialogue.danteNextApp == "DANTE_APP3A")
        {
            danteResultString = "Dante successfully managed to impress the crowd.";
        }
        else if (dialogue.danteNextApp == "DANTE_APP3B")
        {
            danteResultString = "Dante did not manage to impress the crowd.";
        }

    }
    public void fillIndraResults()
    {
        //function checks where indra is going next during the reults screen
        //indra reults get filled depending on what subappointment is next
    }
    public void fillMeiLinResults()
    {
        //function checks where mei lin is going next during the reults screen
        //mei lin reults get filled depending on what subappointment is next
    }
    public void fillPoppyResults()
    {
        //function checks where poppy is going next during the reults screen
        //poppy reults get filled depending on what subappointment is next
    }
    public void fillFullResults()
    {
        //Adds Every character's results to the full result string
        fullResultString = danteResultString + "/" + indraResultString + "/" + meilinResultString + "/" + poppyResultString;
        fullResultString = fullResultString.Replace("/", System.Environment.NewLine);
    }
}
