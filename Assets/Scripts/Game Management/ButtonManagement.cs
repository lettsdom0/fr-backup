﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManagement : MonoBehaviour
{
    //Might need this portion of script for later version of game,
    // for now comment out and update later:
    //flowchart that variables are taken from

    //variables
    public string worstOption;

    /*
    Keep this set of variables and functions in case we want to have the ability to save the game later.
    //variables we want saved
    private int indra_body = 0;
    private int indra_charisma = 0;
    private int indra_intellect = 0;
    private int indra_will = 0;
    public string indra_Appointment = "";
    private int dante_body = 0;
    private int dante_charisma = 0;
    private int dante_intellect = 0;
    private int dante_will = 0;
    public string dante_Appointment = "";


      

    void Start()
    {
        //gets the variables we want
        indra_Appointment = flowchart.GetStringVariable("indra_Appointment");
        indra_body = flowchart.GetIntegerVariable("indra_Body");
        indra_charisma = flowchart.GetIntegerVariable("indra_Charisma");
        indra_intellect = flowchart.GetIntegerVariable("indra_Intellect");
        indra_will = flowchart.GetIntegerVariable("indra_Will");
        dante_Appointment = flowchart.GetStringVariable("dante_Appointment");
        dante_body = flowchart.GetIntegerVariable("dante_Body");
        dante_charisma = flowchart.GetIntegerVariable("dante_Charisma");
        dante_intellect = flowchart.GetIntegerVariable("dante_Intellect");
        dante_will = flowchart.GetIntegerVariable("dante_Will");
    }
    

    public void Save()
    {
        //saves the variables we want saved into player preferences
        PlayerPrefs.SetString("indraKey", indra_Appointment);
        PlayerPrefs.SetString("danteKey", dante_Appointment);
        PlayerPrefs.SetInt("indraKeyB", indra_body);
        PlayerPrefs.SetInt("indraKeyC", indra_charisma);
        PlayerPrefs.SetInt("indraKeyI", indra_intellect);
        PlayerPrefs.SetInt("indraKeyW", indra_will);
        PlayerPrefs.SetInt("danteKeyB", dante_body);
        PlayerPrefs.SetInt("danteKeyC", dante_charisma);
        PlayerPrefs.SetInt("danteKeyI", dante_intellect);
        PlayerPrefs.SetInt("danteKeyW", dante_will);
    }
    public void Load()
    {
        //loads saved varables into game if the keys currently exist
        indra_Appointment = PlayerPrefs.GetString("indraKey", "");
        dante_Appointment = PlayerPrefs.GetString("danteKey", "");
        indra_body = PlayerPrefs.GetInt("indraKeyB", 0);
        indra_charisma = PlayerPrefs.GetInt("indraKeyC", 0);
        indra_intellect = PlayerPrefs.GetInt("indraKeyI", 0);
        indra_will = PlayerPrefs.GetInt("indraKeyW", 0);
        dante_body = PlayerPrefs.GetInt("danteKeyB", 0);
        dante_charisma = PlayerPrefs.GetInt("danteKeyC", 0);
        dante_intellect = PlayerPrefs.GetInt("danteKeyI", 0);
        dante_will = PlayerPrefs.GetInt("danteKeyW", 0);

        // sets flowchart variables to saved counterparts 
        flowchart.SetStringVariable("indra_Appointment", indra_Appointment);
        flowchart.SetIntegerVariable("indra_Body", indra_body);
        flowchart.SetIntegerVariable("indra_Charisma", indra_charisma);
        flowchart.SetIntegerVariable("indra_Intellect", indra_intellect);
        flowchart.SetIntegerVariable("indra_Will", indra_will);
        flowchart.SetStringVariable("dante_Appointment", dante_Appointment);
        flowchart.SetIntegerVariable("dante_Body", dante_body);
        flowchart.SetIntegerVariable("dante_Charisma", dante_charisma);
        flowchart.SetIntegerVariable("dante_Intellect", dante_intellect);
        flowchart.SetIntegerVariable("dante_Will", dante_will);
    }
    public void Delete()
    {
        //deletes key data
        PlayerPrefs.DeleteKey("indraKey");
        PlayerPrefs.DeleteKey("danteKey");
        PlayerPrefs.DeleteKey("indraKeyB");
        PlayerPrefs.DeleteKey("indraKeyC");
        PlayerPrefs.DeleteKey("indraKeyI");
        PlayerPrefs.DeleteKey("indraKeyW");
        PlayerPrefs.DeleteKey("danteKeyB");
        PlayerPrefs.DeleteKey("danteKeyC");
        PlayerPrefs.DeleteKey("danteKeyI");
        PlayerPrefs.DeleteKey("danteKeyW");
    }
    */
    void Start()
    {
        //gets the variables we want

    }

    public void denyOnClick()
    {
        //once the option is clicked, the button will set the potion given boolean to true
        //afterwards it will set the worst possible option boolean to true (Or let the character's current highest stat determine what results appointment they go to) 
        //flowchart.SetBooleanVariable("PotionGiven", true);

        //flowchart.SetBooleanVariable(worstOption, true);
    }

}

