﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timelineSpawner : MonoBehaviour
{
    public GameObject[] timelines;
    public GameObject timeline;
    Timeline timelineScript;
    public int numOfTimelines = 0;

    // Start is called before the first frame update
    public void Start()
    {
        timelines = GameObject.FindGameObjectsWithTag("TimelineCanvas");
        if (timelines.Length <= 0)
        {
            Instantiate(timeline, new Vector3(0, 0, 0), Quaternion.identity);
        }
        timelineScript = timeline.GetComponentInChildren<Timeline>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
