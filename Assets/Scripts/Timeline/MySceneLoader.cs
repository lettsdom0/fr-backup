﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneLoader : MonoBehaviour
{

    public GameObject[] portraits;
    public GameObject[] markers;
    public GameObject[] weekdays;
    public PortraitScript portraitScript;

    //Flowchart flowchart;
    GameObject character;
    Timeline timeline;

    void Start()
    {
        portraits = GameObject.FindGameObjectsWithTag("Portrait");
        markers = GameObject.FindGameObjectsWithTag("Marker");
        weekdays = GameObject.FindGameObjectsWithTag("Day");
        timeline = GameObject.FindGameObjectWithTag("Timeline").GetComponent<Timeline>();

        //flowchart = GameObject.FindGameObjectWithTag("Flowchart").GetComponent<Flowchart>();
        if (character == null)
        {
            character = GameObject.FindWithTag("Character");
        }
    }

    public void checkMarker() {
        int counter = 0;
        foreach (GameObject day in weekdays) {
            if (weekdays[counter].transform.GetChild(1).gameObject.activeInHierarchy == true)
            {
                portraitScript = weekdays[counter].transform.GetChild(2).gameObject.GetComponent<PortraitScript>();
                counter += 1;
            }
            else {
                //print(weekdays[counter].transform.GetChild(1));
                counter += 1;
                //print(counter);
            }
        }
        loadScene();
    }

    void loadScene() {
        SceneManager.LoadScene(portraitScript.nextApp);
        timeline.moveMarker();
    }

}
