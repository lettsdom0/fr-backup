﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class potionLogic : MonoBehaviour
{
    //potion's primary stat for 
    //simple name because primary is indicated from the beginning
    public int primaryStat;

    // secondary stat needs better labling for when adding onto character stats happens
    public int secondaryStat;

    //strings for comparing variables in script to grabbed vairables variables 
    public string primary;
    public string secondary;

    //Variable for holding mana cost
    public int manaCost = 0;

    
    //replacement bool for potion given
    public bool potionHasBeenGiven;

    //bool for second potion given if duo appointment
    public bool secondPotionHasBeenGiven;

    //replacement bool for potion variables
    public bool bodyGiven;
    public bool intGiven;
    public bool charGiven;
    public bool willGiven;

    //object references
    public GameObject controller;
    public GameObject character;
    Actor actorScript;
    ManaCalculator manaCalculator;
    SetStatText statTextSetter;
    public TextMeshProUGUI potionTypeTextUIComp;
    public TextMeshProUGUI potionCostTextUIComp;
    AudioScript audioScript;
    AudioSource audioSource;
    DialogueHandler dialogueHandler;
    Director director;


    string target = "";
    string body_str_var = "";
    string int_str_var = "";
    string char_str_var = "";
    string will_str_var = "";
    //stats we want to change

    private int character_body = 0;
    private int character_charisma = 0;
    private int character_intellect = 0;
    private int character_will = 0;

    //duo appointment boolean
    bool isDuoAppointment;

    //customers in the shop
    public int customersInShop = 1;


    // Start is called before the first frame update
    void Start()
    {
        potionHasBeenGiven = false;

        secondPotionHasBeenGiven = false;

        dialogueHandler = GameObject.FindGameObjectWithTag("GameController").GetComponent<DialogueHandler>();

        bodyGiven = false;
        intGiven = false;
        charGiven = false;
        willGiven = false;
        if (controller == null)
        {
            controller = GameObject.FindWithTag("GameController");
        }
        if (character == null)
        {
            character = GameObject.FindWithTag("Character");
        }
        if (actorScript == null)
        {
            actorScript = character.GetComponent<Actor>();
        }

        //grabs the mana calculator from the game controller
        statTextSetter = GameObject.FindGameObjectWithTag("Stats Tab").GetComponent<SetStatText>();
        //grabs the random potion maker from the game controller
        manaCalculator = controller.GetComponent<ManaCalculator>();
        audioScript = controller.GetComponent<AudioScript>();
        director = controller.GetComponent<Director>();
        SetPotionTypeText();
        SetPotionCostText();

        audioSource = GetComponent<AudioSource>();
        dialogueHandler.isOrIsNotTutorial();
    }


    void OnMouseDown()
    {
        // this object was clicked - Set stats to character
        //currently not working
        setStatsToCharacter();
        Destroy(this.gameObject);
    }


    public void SetPotionTypeText()
    {
        // Find the matching text UI component
        var potionTypeTextUIComp = this.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>(); //GameObject.Find(nameFinder).GetComponent<TMPro.TextMeshProUGUI>();
        potionTypeTextUIComp.text = " ";
        if ((secondaryStat != 0) && (this.tag == "Will"))
        {
            potionTypeTextUIComp.text = "Tranquility:" + primaryStat + "/" + secondary + ": " + secondaryStat;
            potionTypeTextUIComp.text = potionTypeTextUIComp.text.Replace("/", System.Environment.NewLine);
        }
        else if ((secondaryStat != 0) && (this.tag != "Will") && (secondary != "Will"))
        {
            potionTypeTextUIComp.text = primary + ": " + primaryStat + "/" + secondary + ": " + secondaryStat;
            potionTypeTextUIComp.text = potionTypeTextUIComp.text.Replace("/", System.Environment.NewLine);
        }
        else if ((secondaryStat != 0) && (this.tag != "Will") && (secondary == "Will"))
        {
            potionTypeTextUIComp.text = primary + ": " + primaryStat + "/" + "Tranquility:" + secondaryStat;
            potionTypeTextUIComp.text = potionTypeTextUIComp.text.Replace("/", System.Environment.NewLine);
        }
        else if ((secondaryStat == 0) && (this.tag == "Will"))
        {
            potionTypeTextUIComp.text = "Tranquility:" + primaryStat;
        }
        else
        {
            potionTypeTextUIComp.text = primary + ": " + primaryStat;
        }
        //potionTypeTextUIComp.text = primary + ":" + primaryStat + "/" + secondary + ":" + secondaryStat;
        //potionTypeTextUIComp.text = potionTypeTextUIComp.text.Replace("/", System.Environment.NewLine);
    }
    public void failSafeSetPotionTypeText()
    {
        //occurs when multiple of the same potion type appear
        //helps set those potions names

    }
    
   

    public void SetPotionCostText()
    {
        // Find the matching text UI component
        var potionCostTextUIComp = this.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>(); //GameObject.Find(costFinder).GetComponent<TMPro.TextMeshProUGUI>();

        potionCostTextUIComp.text = " ";
        potionCostTextUIComp.text = "Costs: " + manaCost;
    }




    // when called, potion will copy its stats over to the character's stats by utilizing te  
    public void setStatsToCharacter()
    {

        dialogueHandler.OnClickChoiceButton(this.gameObject);
        var currentTarget = actorScript;
        if (character.name == "Indra")
        {
            target = "indra";
            body_str_var = target + "_Body";
            int_str_var = target + "_Intellect";
            char_str_var = target + "_Charisma";
            will_str_var = target + "_Will";
            repeatCustomer();
        }
        else if (character.name == "Dante")
        {
            target = "dante";
            body_str_var = target + "_Body";
            int_str_var = target + "_Intellect";
            char_str_var = target + "_Charisma";
            will_str_var = target + "_Will";

            repeatCustomer();

        }
        else if (character.name == "Mei Lin")
        {
            target = "meilin";
            body_str_var = target + "_Body";
            int_str_var = target + "_Intellect";
            char_str_var = target + "_Charisma";
            will_str_var = target + "_Will";

            repeatCustomer();

        }
        else if (character.name == "Poppy" ||character.name == "Zinnia" || character.name == "Poppy and Zinnia")
        {
            print("Found Poppy");
            target = "poppy";
            body_str_var = target + "_Body";
            int_str_var = target + "_Intellect";
            char_str_var = target + "_Charisma";
            will_str_var = target + "_Will";

            repeatCustomer();

        }
        
    }


    void potionGiven()
    {
        //sets stats based on tag of potion object
        //if no tag it will say so
        if (this.tag == "Body")
        {
            character_body += primaryStat; //Sets body stat first

            if (secondary == "Intellect")
                character_intellect += secondaryStat; // sets intellect stat second if set to that
            else if (secondary == "Charisma")
                character_charisma += secondaryStat; // sets charisma stat second if set to that
            else if (secondary == "Will")
                character_will += secondaryStat; // sets will stat second if set to that

        }
        else if (this.tag == "Intellect")
        {
            character_intellect += primaryStat;

            if (secondary == "Body")
                character_body += secondaryStat;
            else if (secondary == "Charisma")
                character_charisma += secondaryStat;
            else if (secondary == "Will")
                character_will += secondaryStat;
        }
        else if (this.tag == "Charisma")
        {
            character_charisma += primaryStat;

            if (secondary == "Body")
                character_body += secondaryStat;
            else if (secondary == "Intellect")
                character_intellect += secondaryStat;
            else if (secondary == "Will")
                character_will += secondaryStat;
        }
        else if (this.tag == "Will")
        {
            character_will += secondaryStat;

            if (secondary == "Body")
                character_body += secondaryStat;
            else if (secondary == "Intellect")
                character_intellect += secondaryStat;
            else if (secondary == "Charisma")
                character_charisma += secondaryStat;
        }

        else
        {
            print("No tag detected");
        }
        //checks if mana is present so not to break scenes without manacalculator
        //inside of the controller
        if (manaCalculator != null)
        {
            manaCalculator.subtractPlayerMana(manaCost);
            manaCalculator.SubtractTextUIMana(manaCost);
        }
        //immediately sets the customer's stats after they're set.
        actorScript.Body = character_body;
        actorScript.Intellect = character_intellect;
        actorScript.Charisma = character_charisma;
        actorScript.Will = character_will;

        director.updateMasterStats();

        statTextSetter.getStatsForChar();

        //play the potion collection sound
        audioSource.Play();
    }
    void repeatCustomer()
    {
        //checks potion given variable before moving forward
        //if potion has been given already, invis text lets the player know
        //if potion has not been given yet, proceed to potionGiven
        var potionDeterminer = potionHasBeenGiven;
        var manaDeterminer = manaCalculator.checkPlayerMana();
        if ((potionDeterminer == false) && (manaDeterminer > 0) && (manaDeterminer >= manaCost))
        {
            if (this.tag == "Body")
            {
                //prevents scipt from breaking if manacalculator isn't present
                bodyGiven = true;

            }
            else if (this.tag == "Intellect")
            {
                //prevents scipt from breaking if manacalculator isn't present
                intGiven = true;

            }
            else if (this.tag == "Charisma")
            {
                //prevents scipt from breaking if manacalculator isn't present
                charGiven = true;

            }
            else if (this.tag == "Will")
            {
                //prevents scipt from breaking if manacalculator isn't present
                willGiven = true;
            }
            potionHasBeenGiven = true;
            potionGiven();
        }
        else if ((potionDeterminer == false) && (manaDeterminer > 0) && (manaDeterminer < manaCost))
        {

            manaCalculator.noMoMana();

        }
        else if ((potionDeterminer == false) && (manaDeterminer <= 0) && (manaDeterminer < manaCost))
        {

            manaCalculator.noMoMana();

        }
        else
        {

            manaCalculator.onePerCustomer();

        }
    }
}
