﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlusMinusButton : MonoBehaviour
{

    public Inventory inventory;

    public GameObject Body;
    public GameObject Will;
    public GameObject Intelligence;
    public GameObject Charisma;

    public TextMeshProUGUI points;
    public TextMeshProUGUI bodyText;
    public TextMeshProUGUI willText;
    public TextMeshProUGUI intelligenceText;
    public TextMeshProUGUI charismaText;

    int body;
    public int will;
    int intelligence;
    int charisma ;

    private void Start()
    {
        body = 1;
        will = 1;
        intelligence = 1;
        charisma = 1;
    }

    public void Plus() {
        if (inventory.charge > 0)
        {
            GameObject button = EventSystem.current.currentSelectedGameObject;
            if (button.tag == "Body")
            {
                body += 1;
                inventory.charge -= 1;
                bodyText.text = "+" + body;
            }
            else if (button.tag == "Willpower")
            {
                will += 1;
                inventory.charge -= 1;
                willText.text = "+" + will;
                print(will);
            }
            else if (button.tag == "Intellect")
            {
                intelligence += 1;
                inventory.charge -= 1;
                intelligenceText.text = "+" + intelligence;
            }
            else if (button.tag == "Charisma")
            {
                charisma += 1;
                inventory.charge -= 1;
                charismaText.text = "+" + charisma;
            }
            points.text = "Points: " + inventory.charge;
        }
    }

    public void Minus() {
        if (inventory.charge < 8)
        {
            GameObject button = EventSystem.current.currentSelectedGameObject;
            if (button.tag == "Body")
            {
                body -= 1;
                inventory.charge += 1;
                bodyText.text = "+" + body;
            }
            else if (button.tag == "Willpower")
            {
                will -= 1;
                inventory.charge += 1;
                willText.text = "+" + will;
                print(will);
            }
            else if (button.tag == "Intellect")
            {
                intelligence -= 1;
                inventory.charge += 1;
                intelligenceText.text = "+" + intelligence;
            }
            else if (button.tag == "Charisma")
            {
                charisma -= 1;
                inventory.charge += 1;
                charismaText.text = "+" + charisma;
            }
            points.text = "Points: " + inventory.charge;
        }
    }

    public void Next() {
        print(body);
        print(will);
        print(intelligence);
        print(charisma);
       
    }
}
